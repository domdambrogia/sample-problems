<?php

/**
 * Given an array of unsorted integers and a sum, find the amount of
 * combinations of integers that have the sum of $sum.
 *
 * !!! Bonus points for for doing in O(n) time !!!
 *
 * @param array $arr
 * @param int $sum
 * @return array
 */
function getNumPairsForSum(array $arr, int $sum)
{
    /**
     * Fill this out!
     * If you need some help, take a look at the tests below.
     */
}

/**
 * Test getPairs method.
 * @throws Exception on failure
 * @return string
 */
function assertSuccess()
{
    $status = "Success!";

    $tests = array([
        'expected' => 0,
        'given' => getNumPairsForSum([], 0)
    ], [
        'expected' => 0,
        'given' => getNumPairsForSum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 20)
    ], [
        'expected' => 1,
        'given' => getNumPairsForSum([10, 15, 3, 7], 17)
    ], [
        'expected' => 2,
        'given' => getNumPairsForSum([1, 3, 5, 8, 10], 11)
    ], [
        'expected' => 3,
        'given' => getNumPairsForSum([1, 4, 7, 7, 10, 13], 14)
    ]);

    foreach ($tests as $i => $t) {
        $e = $t['expected'];
        $g = $t['given'];
        $testNum = $i + 1;

        if ($e !== $g) {
            $status = "Failed test {$testNum}! Expected: {$e} Given: {$g}";
            break;
        }
    }

    return $status;
}

print"Test status: " . assertSuccess();
