/**
 * Given an array of unsorted integers and a sum, find the amount of
 * combinations of integers that have the sum of $sum.
 *
 * !!! Bonus points for for doing in O(n) time !!!
 *
 * @param array $arr
 * @param int $sum
 * @return array
 */
function getNumPairsForSum(arr, sum) {
  var hash = {};
  var pairs = 0;

  for (var i = 0; i < arr.length; i++) {
    var curr = arr[i];
    var targetDiff = sum - curr;
    pairs = typeof hash[targetDiff] !== 'undefined' ? pairs + 1 : pairs;
    hash[curr] = true;
  }

  return pairs;
}

/**
 * Test getPairs method.
 * @return string
 */
function assertSuccess() {
  var status = '';

  var tests = [{
    expected: 0,
    given: getNumPairsForSum([], 0)
  }, {
    expected: 0,
    given: getNumPairsForSum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 20)
  }, {
    expected: 1,
    given: getNumPairsForSum([10, 15, 3, 7], 17)
  }, {
    expected: 2,
    given: getNumPairsForSum([1, 3, 5, 8, 10], 11)
  }, {
    expected: 3,
    given: getNumPairsForSum([1, 4, 7, 7, 10, 13], 14)
  }];

  for (var i = 0; i < tests.length; i++) {
    var t = tests[i];

    status += '\n';

    if (t.expected === t.given) {
      status += `${i+1} - Passed`;
    } else {
      status += `${i+1} - Failed: Expected: ${t.expected}. Given: ${t.given}`;
    }
  }

  return status;
}

console.log(`Test status: ${assertSuccess()}`);
